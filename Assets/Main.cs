using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Main : MonoBehaviour
{
    public static string texturePath = "";
    public static string listPath = "";

    public GameObject PS1Game_standard;
    public GameObject PS1Game_double;
    public GameObject PS2Game;
    public GameObject PS3Game_original;
    public GameObject PS3Game_new;
    public GameObject GCGame;
    public GameObject WIIGame;
    public GameObject WIIUGame;
    public GameObject XBOXGame;
    public GameObject X360Game;
    public Vector3 startPos = new Vector3(0, 0, 0);
    private float x = 0;
    private float y = 0;
    public float xToMeters = 1;

    void Start()
    {
        listPath = Application.dataPath + "";
        texturePath = Application.dataPath + "";

        List<string[]> gameListPS1 = SetGameList("PS1");
        InstantiateBoxes(gameListPS1);

        List<string[]> gameListPS2 = SetGameList("PS2");
        InstantiateBoxes(gameListPS2);

        List<string[]> gameListPS3 = SetGameList("PS3");
        InstantiateBoxes(gameListPS3);

        List<string[]> gameListGC = SetGameList("GC");
        InstantiateBoxes(gameListGC);

        List<string[]> gameListWII = SetGameList("WII");
        InstantiateBoxes(gameListWII);

        List<string[]> gameListWIIU = SetGameList("WIIU");
        InstantiateBoxes(gameListWIIU);

        List<string[]> gameListXBOX = SetGameList("XBOX");
        InstantiateBoxes(gameListXBOX);

        List<string[]> gameListX360 = SetGameList("X360");
        InstantiateBoxes(gameListX360);
    }

    List<string[]> SetGameList(string listName)
    {
        List<string[]> gameList = new List<string[]>();

        if (File.Exists(listPath + "\\" + listName + ".csv"))
        {
            string[] lines = File.ReadAllLines(listPath + "\\" + listName + ".csv", System.Text.Encoding.GetEncoding(1252));
            foreach (string line in lines)
                gameList.Add(line.Split(';'));
            gameList.RemoveAt(0);
        }
        else
            Debug.Log("File not found: " + listPath + "\\" + listName + ".csv");
        return gameList;
    }

    void InstantiateBoxes(List<string[]> gameList)
    {
        foreach(string[] game in gameList)
        {
            InstantiateBox(game);
        }
        y += 19 * xToMeters;
        x = 0;
    }

    void InstantiateBox(string[] game)
    {
        GameObject newGame = null;

        if (game[5].Contains("PS2"))
        {
            newGame = Instantiate(PS2Game, startPos + new Vector3(x, y, 0), transform.rotation);

            PS2Game ps2Game = newGame.GetComponent<PS2Game>();
            ps2Game.caseType = game[5];
            ps2Game.gameTitle = game[2];
            ps2Game.gameSerial = game[0];
            ps2Game.gameSubSerial = game[1];
            ps2Game.Set();
            x += ps2Game.width * xToMeters;
        }
        else if (game[5].Contains("PS3"))
        {
            if (game[5] == "PS3 [Original]")
                newGame = Instantiate(PS3Game_original, startPos + new Vector3(x, y, 0), transform.rotation);
            else
                newGame = Instantiate(PS3Game_new, startPos + new Vector3(x, y, 0), transform.rotation);

            PS3Game ps3Game = newGame.GetComponent<PS3Game>();
            ps3Game.caseType = game[5];
            ps3Game.gameTitle = game[2];
            ps3Game.gameSerial = game[0];
            ps3Game.gameSubSerial = game[1];
            ps3Game.Set();
            x += ps3Game.width * xToMeters;
        }
        else if (game[5].Contains("PS1"))
        {
            if (game[5].Contains("Double"))
                newGame = Instantiate(PS1Game_double, startPos + new Vector3(x, y, 0), transform.rotation);
            else
                newGame = Instantiate(PS1Game_standard, startPos + new Vector3(x, y, 0), transform.rotation);

            PS1Game ps1Game = newGame.GetComponent<PS1Game>();
            ps1Game.caseType = game[5];
            ps1Game.gameTitle = game[2];
            ps1Game.gameSerial = game[0];
            ps1Game.gameSubSerial = game[1];
            ps1Game.gameNbDiscs = game[6];
            ps1Game.gameFontSize = int.Parse(game[7]);
            ps1Game.Set();
            x += ps1Game.width * xToMeters;
        }
        else if (game[5].Contains("WIIU"))
        {
            newGame = Instantiate(WIIUGame, startPos + new Vector3(x, y, 0), transform.rotation);

            WIIUGame wiiUGame = newGame.GetComponent<WIIUGame>();
            wiiUGame.caseType = game[5];
            wiiUGame.gameTitle = game[2].Replace("\\n", "\n");
            wiiUGame.gameSerial = game[0];
            wiiUGame.Set();
            x += wiiUGame.width * xToMeters;
        }
        else if (game[5].Contains("WII"))
        {
            newGame = Instantiate(WIIGame, startPos + new Vector3(x, y, 0), transform.rotation);

            WIIGame wiiGame = newGame.GetComponent<WIIGame>();
            wiiGame.caseType = game[5];
            wiiGame.gameTitle = game[2];
            wiiGame.gameSerial = game[0];
            wiiGame.Set();
            x += wiiGame.width * xToMeters;
        }
        else if (game[5].Contains("GC"))
        {
            newGame = Instantiate(GCGame, startPos + new Vector3(x, y, 0), transform.rotation);

            GCGame gcGame = newGame.GetComponent<GCGame>();
            gcGame.caseType = game[5];
            gcGame.gameTitle = game[2].Replace("\\n", "\n");
            gcGame.gameSerial = game[0];
            gcGame.Set();
            x += gcGame.width * xToMeters;
        }
        else if (game[5].Contains("X360"))
        {
            newGame = Instantiate(X360Game, startPos + new Vector3(x, y, 0), transform.rotation);

            X360Game x360Game = newGame.GetComponent<X360Game>();
            x360Game.caseType = game[5];
            x360Game.gameTitle = game[2].Replace("\\n", "\n");
            x360Game.gameSerial = game[0];
            x360Game.Set();
            x += x360Game.width * xToMeters;
        }
        else if (game[5].Contains("XBOX"))
        {
            newGame = Instantiate(XBOXGame, startPos + new Vector3(x, y, 0), transform.rotation);

            XBOXGame xboxGame = newGame.GetComponent<XBOXGame>();
            xboxGame.caseType = game[5];
            xboxGame.gameTitle = game[2].Replace("\\n", "\n");
            xboxGame.gameSerial = game[0];
            xboxGame.Set();
            x += xboxGame.width * xToMeters;
        }
    }
}
