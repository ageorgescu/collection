using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GCGame : MonoBehaviour
{
    [Header("Game Properties")]
    public string caseType = "";
    public float height, length, width;
    public string gameTitle, gameSerial;

    [Header("Box Properties")]
    public Texture gameFront;
    public Texture gameBack;
    public Texture gameSpine;
    public Color gameTitleColor;
    public Color boxColor;

    [Header("Components")]
    public Text gameTitle_text;
    public Material gameFront_mat, gameBack_mat, gameSpine_mat, gameBox_mat;
    public MeshRenderer meshRenderer;

    [Header("Spines")]
    public Texture spineWhitePink;
    public Texture spineWhiteOrange;
    public Texture spineWhiteRed;
    public Texture spineWhiteLavander;
    public Texture spineWhiteGreen;
    public Texture spineBlackPink;
    public Texture spineBlackOrange;
    public Texture spinePlayerPink;
    public Texture spinePlayerRed;
    public Texture spineStandard;

    [Header("Text Colors")]
    public Color textColorStandard;
    public Color textColorSunshine;
    public Color textColorWhite;

    [Header("Box Colors")]
    public Color boxColorSunshine;
    public Color boxColorPlayer;
    public Color boxColorWWL;
    public Color boxColorBlack;

    bool customSpine;

    internal void Set()
    {
        SetImages();
        SetColor();
        SetBoxColor();
        SetMaterials();
        SetText();
    }

    void SetMaterials()
    {
        meshRenderer.materials[0] = new Material(gameBox_mat);
        meshRenderer.materials[1] = new Material(gameSpine_mat);
        meshRenderer.materials[2] = new Material(gameBack_mat);
        meshRenderer.materials[3] = new Material(gameFront_mat);

        if (gameSpine)
            meshRenderer.materials[1].mainTexture = gameSpine;
        if (gameBack)
            meshRenderer.materials[2].mainTexture = gameBack;
        if (gameFront)
            meshRenderer.materials[3].mainTexture = gameFront;
        meshRenderer.materials[0].color = boxColor;
    }

    void SetText()
    {
        gameTitle_text.color = gameTitleColor;

        if (customSpine)
            gameTitle_text.text = "";
        else
            gameTitle_text.text = gameTitle;
    }

    Texture LoadTexture(string path)
    {
        Texture2D tex = null;

        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }

        return tex;
    }

    void SetImages()
    {
        if (File.Exists(Main.texturePath + "\\GC\\Front\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\GC\\Front\\" + gameSerial + ".jpg found");
            gameFront = LoadTexture(Main.texturePath + "\\GC\\Front\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\GC\\Front\\" + gameSerial + ".jpg not found");
            gameFront = null;
        }

        if (File.Exists(Main.texturePath + "\\GC\\Back\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\GC\\Back\\" + gameSerial + ".jpg found");
            gameBack = LoadTexture(Main.texturePath + "\\GC\\Back\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\GC\\Back\\" + gameSerial + ".jpg not found");
            gameBack = null;
        }

        SetSpine();
    }

    void SetSpine()
    {
        if (File.Exists(Main.texturePath + "\\GC\\Spine\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\GC\\Spine\\" + gameSerial + ".jpg found");
            gameSpine = LoadTexture(Main.texturePath + "\\GC\\Spine\\" + gameSerial + ".jpg");
            customSpine = true;
        }
        else
        {
            Debug.Log(Main.texturePath + "\\GC\\Spine\\" + gameSerial + ".jpg not found");

            if (caseType == "GC [Standard-White-Pink]")
                gameSpine = spineWhitePink;
            else if (caseType == "GC [Standard-White-Orange]")
                gameSpine = spineWhiteOrange;
            else if (caseType == "GC [Standard-White-Red]")
                gameSpine = spineWhiteRed;
            else if (caseType == "GC [Standard-White-Lavander]")
                gameSpine = spineWhiteLavander;
            else if (caseType == "GC [Standard-White-Green]")
                gameSpine = spineWhiteGreen;
            else if (caseType == "GC [Standard-Black-Pink]")
                gameSpine = spineBlackPink;
            else if (caseType == "GC [Standard-Black-Orange]")
                gameSpine = spineBlackOrange;
            else if (caseType == "GC [Standard-Black-Red]")
                gameSpine = spineBlackOrange;
            else if (caseType == "GC [Player's Choice-Pink]")
                gameSpine = spinePlayerPink;
            else if (caseType == "GC [Player's Choice-Red]")
                gameSpine = spinePlayerRed;
            else
                gameSpine = spineStandard;
        }
    }

    void SetColor()
    {
        Debug.Log(caseType);
        if (caseType == "GC [Super Mario Sunshine]")
            gameTitleColor = textColorSunshine;
        else if (caseType == "GC [Player's Choice-Pink]" || caseType == "GC [Player's Choice-Red]")
            gameTitleColor = textColorWhite;
        else if (caseType == "GC [Wind Waker Limited]")
            gameTitleColor = textColorWhite;
        else if (caseType == "GC [Standard-Black-Pink]" || caseType == "GC [Standard-Black-Orange]")
            gameTitleColor = textColorWhite;
        else
            gameTitleColor = textColorStandard;
    }

    void SetBoxColor()
    {
        if (caseType == "GC [Super Mario Sunshine]")
            boxColor = boxColorSunshine;
        else if (caseType.Contains("Player's Choice"))
            boxColor = boxColorPlayer;
        else if (caseType == "GC [Wind Waker Limited]")
            boxColor = boxColorWWL;
        else
            boxColor = boxColorBlack;
    }
}
