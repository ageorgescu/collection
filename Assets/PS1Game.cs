using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PS1Game : MonoBehaviour
{
    [Header("Game Properties")]
    public string caseType = "";
    public float height, length, width;
    public string gameTitle, gameSerial, gameSubSerial, gameNbDiscs;

    [Header("Box Properties")]
    public Texture gameFront;
    public Texture gameBack;
    public Texture gameSpine;
    public Color gameTitleColor;
    public Color boxColor;

    [Header("Components")]
    public Text gameTitle_text;
    public Text gameTitle2_text;
    public Text gameTitle3_text;
    public Text gameTitle4_text;
    public Text gameSerial_text;
    public Text gameSerial2_text;
    public Text gameSubSerial_text;
    public Text gameSubSerial2_text;
    public Text gameNbDiscs_text;
    public Text gameNbDiscs2_text;
    public Material gameFront_mat, gameBack_mat, gameSpine_mat, gameBox_mat;
    public MeshRenderer meshRenderer;

    [Header("Spines")]
    public Texture spineStandard;
    public Texture spinePlatinum;
    public Texture spineDoubleStandard;
    public Texture spineDoublePlatinum;

    [Header("Text Colors")]
    public Color textColorStandard;
    public Color textColorPlatinum;

    [Header("Box Colors")]
    public Color boxColorStandard;
    public Color boxColorGrey;

    bool customSpine;
    internal int gameFontSize = 60;

    internal void Set()
    {
        SetImages();
        SetColor();
        SetBoxColor();
        SetMaterials();
        SetText();
    }

    void SetMaterials()
    {
        meshRenderer.materials[3] = new Material(gameBox_mat);
        meshRenderer.materials[1] = new Material(gameSpine_mat);
        meshRenderer.materials[0] = new Material(gameBack_mat);
        meshRenderer.materials[4] = new Material(gameFront_mat);

        if (gameSpine)
            meshRenderer.materials[1].mainTexture = gameSpine;
        if (gameBack)
            meshRenderer.materials[0].mainTexture = gameBack;
        if (gameFront)
            meshRenderer.materials[4].mainTexture = gameFront;
        meshRenderer.materials[3].color = boxColor;
    }

    void SetText()
    {
        gameTitle_text.color = gameTitleColor;
        gameTitle2_text.color = gameTitleColor;
        gameTitle3_text.color = gameTitleColor;
        gameTitle4_text.color = gameTitleColor;
        gameSerial_text.color = gameTitleColor;
        gameSerial2_text.color = gameTitleColor;
        gameSubSerial_text.color = gameTitleColor;
        gameSubSerial2_text.color = gameTitleColor;
        gameNbDiscs_text.color = gameTitleColor;
        gameNbDiscs2_text.color = gameTitleColor;

        if (customSpine)
        {
            gameTitle_text.text = "";
            gameTitle2_text.text = "";
            gameTitle3_text.text = "";
            gameTitle4_text.text = "";
            gameSerial_text.text = "";
            gameSerial2_text.text = "";
            gameSubSerial_text.text = "";
            gameSubSerial2_text.text = "";
            gameNbDiscs_text.text = "";
            gameNbDiscs2_text.text = "";
        }

        else
        {
            gameTitle_text.text = gameTitle2_text.text = gameTitle3_text.text = gameTitle4_text.text = gameTitle.Replace("\\n", "\n");
            gameSerial_text.text = gameSerial2_text.text = gameSerial.Replace("-", "\n");
            gameSubSerial_text.text = gameSubSerial2_text.text = gameSubSerial != "" ? gameSubSerial.PadLeft(7, '0') : "";
            gameNbDiscs_text.text = gameNbDiscs2_text.text = gameNbDiscs != "1" ? gameNbDiscs + " DISCS" : "";

            if (!caseType.Contains("Double") && gameNbDiscs != "1")
            {
                gameSubSerial2_text.text = "";
            }
        }

        gameTitle_text.resizeTextMaxSize = gameTitle2_text.resizeTextMaxSize = gameTitle3_text.resizeTextMaxSize = gameTitle4_text.resizeTextMaxSize = SetTitleSize(21);
    }

    Texture LoadTexture(string path)
    {
        Texture2D tex = null;

        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }

        return tex;
    }

    void SetImages()
    {
        if (File.Exists(Main.texturePath + "\\PS1\\Front\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS1\\Front\\" + gameSerial + ".jpg found");
            gameFront = LoadTexture(Main.texturePath + "\\PS1\\Front\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS1\\Front\\" + gameSerial + ".jpg not found");
            gameFront = null;
        }

        if (File.Exists(Main.texturePath + "\\PS1\\Back\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS1\\Back\\" + gameSerial + ".jpg found");
            gameBack = LoadTexture(Main.texturePath + "\\PS1\\Back\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS1\\Back\\" + gameSerial + ".jpg not found");
            gameBack = null;
        }

        SetSpine();
    }

    void SetSpine()
    {
        if (File.Exists(Main.texturePath + "\\PS1\\Spine\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS1\\Spine\\" + gameSerial + ".jpg found");
            gameSpine = LoadTexture(Main.texturePath + "\\PS1\\Spine\\" + gameSerial + ".jpg");
            customSpine = true;
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS1\\Spine\\" + gameSerial + ".jpg not found");

            if (caseType == "PS1 [Standard]")
                gameSpine = spineStandard;
            else if (caseType == "PS1 [Platinum]")
                gameSpine = spinePlatinum;
            else if (caseType == "PS1 [Platinum-Gray]")
                gameSpine = spinePlatinum;
            else if (caseType == "PS1 [Double-Standard]")
                gameSpine = spineDoubleStandard;
            else if (caseType == "PS1 [Double-Platinum]")
                gameSpine = spineDoublePlatinum;
            else
                gameSpine = spineStandard;
        }
    }

    void SetColor()
    {
        Debug.Log(caseType);
        if (caseType == "PS1 [Standard]")
            gameTitleColor = textColorStandard;
        else if (caseType == "PS1 [Platinum]")
            gameTitleColor = textColorPlatinum;
        else if (caseType == "PS1 [Platinum-Gray]")
            gameTitleColor = textColorPlatinum;
        else if (caseType == "PS1 [Double-Standard]")
            gameTitleColor = textColorStandard;
        else if (caseType == "PS1 [Double-Platinum]")
            gameTitleColor = textColorPlatinum;
        else
            gameTitleColor = textColorStandard;
    }

    void SetBoxColor()
    {
        if (caseType == "PS1 [Platinum-Gray]" || caseType == "PS1 [Double-Standard]" || caseType == "PS1 [Double-Platinum]")
            boxColor = boxColorGrey;
        else
            boxColor = boxColorStandard;
    }

    int SetTitleSize(int minimalLength)
    {
        /*int index = gameTitle.IndexOf("\\");
        int length = gameTitle.Length;
        if (index == -1 && length > minimalLength)
        {
            return (int)(60 - (length - minimalLength) * 1f);
        }
        else
            return (60);*/
        return (gameFontSize);
    }
}
