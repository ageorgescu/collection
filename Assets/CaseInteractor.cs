using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaseInteractor : MonoBehaviour
{
    Ray ray;
	RaycastHit hit;
	
	void Update()
	{
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if(Physics.Raycast(ray, out hit))
		{
            if (hit.collider == GetComponent<Collider>() && Input.GetMouseButtonDown(0))
				Action();
		}
	}

    bool state;

    void Action()
    {
        if (!state)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.15f);
            state = true;
        }
        else
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.15f);
            state = false;
        }
    }
}