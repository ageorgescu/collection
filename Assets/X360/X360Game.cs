using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class X360Game : MonoBehaviour
{
    [Header("Game Properties")]
    public string caseType = "";
    public float height, length, width;
    public string gameTitle, gameSerial;

    [Header("Box Properties")]
    public Texture gameFront;
    public Texture gameBack;
    public Texture gameSpine;
    public Color gameTitleColor;
    public Color boxColor;

    [Header("Components")]
    public Text gameTitle_text;
    public Material gameFront_mat, gameBack_mat, gameSpine_mat, gameBox_mat;
    public MeshRenderer meshRenderer;

    [Header("Spines")]
    public Texture spineStandard;
    public Texture spineStandardAlt;
    public Texture spineClassicsOld;
    public Texture spineClassicsNew;
    public Texture spineClassicsBS;

    [Header("Text Colors")]
    public Color textColorStandard;
    public Color textColorStandardAlt;
    public Color textColorClassicsOld;
    public Color textColorClassicsNew;
    public Color textColorClassicsBS;

    [Header("Box Colors")]
    public Color boxColorStandard;
    public Color boxColorGrey;

    bool customSpine;

    internal void Set()
    {
        SetImages();
        SetColor();
        SetBoxColor();
        SetMaterials();
        SetText();
    }

    void SetMaterials()
    {
        meshRenderer.materials[0] = new Material(gameBox_mat);
        meshRenderer.materials[1] = new Material(gameSpine_mat);
        meshRenderer.materials[2] = new Material(gameBack_mat);
        meshRenderer.materials[3] = new Material(gameFront_mat);

        if (gameSpine)
            meshRenderer.materials[1].mainTexture = gameSpine;
        if (gameBack)
            meshRenderer.materials[2].mainTexture = gameBack;
        if (gameFront)
            meshRenderer.materials[3].mainTexture = gameFront;
        meshRenderer.materials[0].color = boxColor;
    }

    void SetText()
    {
        gameTitle_text.color = gameTitleColor;

        if (customSpine)
            gameTitle_text.text = "";

        else
            gameTitle_text.text = gameTitle.Replace("\\n", "\n");
    }

    Texture LoadTexture(string path)
    {
        Texture2D tex = null;

        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }

        return tex;
    }

    void SetImages()
    {
        if (File.Exists(Main.texturePath + "\\X360\\Front\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\X360\\Front\\" + gameSerial + ".jpg found");
            gameFront = LoadTexture(Main.texturePath + "\\X360\\Front\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\X360\\Front\\" + gameSerial + ".jpg not found");
            gameFront = null;
        }

        if (File.Exists(Main.texturePath + "\\X360\\Back\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\X360\\Back\\" + gameSerial + ".jpg found");
            gameBack = LoadTexture(Main.texturePath + "\\X360\\Back\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\X360\\Back\\" + gameSerial + ".jpg not found");
            gameBack = null;
        }

        SetSpine();
    }

    void SetSpine()
    {
        if (File.Exists(Main.texturePath + "\\X360\\Spine\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\X360\\Spine\\" + gameSerial + ".jpg found");
            gameSpine = LoadTexture(Main.texturePath + "\\X360\\Spine\\" + gameSerial + ".jpg");
            customSpine = true;
        }
        else
        {
            Debug.Log(Main.texturePath + "\\X360\\Spine\\" + gameSerial + ".jpg not found");

            if (caseType == "X360 [Standard]")
                gameSpine = spineStandard;
            else if (caseType == "X360 [Standard-Alt]")
                gameSpine = spineStandardAlt;
            else if (caseType == "X360 [Classics-Old]")
                gameSpine = spineClassicsOld;
            else if (caseType == "X360 [Classics-New]")
                gameSpine = spineClassicsNew;
            else if (caseType == "X360 [Classics-BS]")
                gameSpine = spineClassicsBS;
            else
                gameSpine = spineStandard;
        }
    }

    void SetColor()
    {
        Debug.Log(caseType);
        if (caseType == "X360 [Standard]")
            gameTitleColor = textColorStandard;
        else if (caseType == "X360 [Standard-Alt]")
            gameTitleColor = textColorStandardAlt;
        else if (caseType == "X360 [Classics-Old]")
            gameTitleColor = textColorClassicsOld;
        else if (caseType == "X360 [Classics-New]")
            gameTitleColor = textColorClassicsNew;
        else if (caseType == "X360 [Classics-BS]")
            gameTitleColor = textColorClassicsBS;
        else
            gameTitleColor = textColorStandard;
    }

    void SetBoxColor()
    {
        if (caseType == "X360 [Standard]")
            boxColor = boxColorStandard;
        else if (caseType == "X360 [Standard-Alt]")
            boxColor = boxColorStandard;
        else if (caseType == "X360 [Classics-Old]")
            boxColor = boxColorGrey;
        else if (caseType == "X360 [Classics-New]")
            boxColor = boxColorStandard;
        else if (caseType == "X360 [Classics-BS]")
            boxColor = boxColorGrey;
        else
            boxColor = boxColorStandard;
    }
}
