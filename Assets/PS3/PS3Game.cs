using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PS3Game : MonoBehaviour
{
    [Header("Game Properties")]
    public string caseType = "";
    public float height, length, width;
    public string gameTitle, gameSerial, gameSubSerial;

    [Header("Box Properties")]
    public Texture gameFront;
    public Texture gameBack;
    public Texture gameSpine;
    public Color gameTitleColor;
    public Color boxColor;
    public float boxSmoothness = 0.75f;

    [Header("Components")]
    public Text gameTitle_text;
    public Text gameTitleBis_text;
    public Text gameSerial_text;
    public Text gameSubSerial_text;
    public Material gameFront_mat, gameBack_mat, gameSpine_mat, gameBox_mat, gameSide_mat;
    public MeshRenderer meshRenderer;

    [Header("Spines")]
    public Texture spineOriginal;
    public Texture spineOriginalAlt;
    public Texture spineOriginalPlatinum;
    public Texture spineNew;
    public Texture spineNewAlt;
    public Texture spineNewPlatinum;
    public Texture spineNewEssentials;

    [Header("Text Colors")]
    public Color textColorOriginal;
    public Color textColorOriginalAlt;
    public Color textColorOriginalPlatinum;
    public Color textColorNew;
    public Color textColorNewAlt;
    public Color textColorNewPlatinum;
    public Color textColorNewEssentials;

    [Header("Box Colors")]
    public Color boxColorStandard;
    public Color boxColorBlack;
    public Color boxColorPlatinum;

    [Header("Box Smoothness")]
    public float boxSmoothStandard;
    public float boxSmoothPlatinum;

    bool customSpine;

    internal void Set()
    {
        SetImages();
        SetColor();
        SetBoxColor();
        SetMaterials();
        SetText();
    }

    void SetMaterials()
    {
        meshRenderer.materials[0] = new Material(gameBox_mat);
        meshRenderer.materials[1] = new Material(gameSpine_mat);
        meshRenderer.materials[2] = new Material(gameBack_mat);
        meshRenderer.materials[3] = new Material(gameFront_mat);
        meshRenderer.materials[4] = new Material(gameSide_mat);

        if (gameSpine)
            meshRenderer.materials[1].mainTexture = gameSpine;
        if (gameBack)
            meshRenderer.materials[2].mainTexture = gameBack;
        if (gameFront)
            meshRenderer.materials[3].mainTexture = gameFront;
        meshRenderer.materials[0].color = boxColor;
        meshRenderer.materials[4].color = boxColor;
        meshRenderer.materials[0].SetFloat("_GlossMapScale", boxSmoothness);
        meshRenderer.materials[4].SetFloat("_GlossMapScale", boxSmoothness);
        Debug.Log(meshRenderer.materials[0].GetFloat("_GlossMapScale"));
    }

    void SetText()
    {
        gameTitle_text.color = gameTitleColor;
        gameTitleBis_text.color = gameTitleColor;
        gameSerial_text.color = gameTitleColor;
        gameSubSerial_text.color = gameTitleColor;

        if (customSpine)
        {
            gameTitle_text.text = "";
            gameTitleBis_text.text = "";
            gameSerial_text.text = "";
            gameSubSerial_text.text = "";
        }

        else
        {
            gameTitle_text.text = gameTitle.Replace("\\n", "\n"); ;
            gameTitleBis_text.text = gameTitle.Replace("\\n", "\n"); ;
            gameSerial_text.text = gameSerial.Replace("-", "\n");
            gameSubSerial_text.text = gameSubSerial;
        }

        if (caseType.Contains("New"))
        {
            gameTitle_text.gameObject.SetActive(false);
            gameTitleBis_text.gameObject.SetActive(true);
        }
        else
        {
            gameTitle_text.gameObject.SetActive(true);
            gameTitleBis_text.gameObject.SetActive(false);
        }
    }

    Texture LoadTexture(string path)
    {
        Texture2D tex = null;

        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }

        return tex;
    }

    void SetImages()
    {
        if (File.Exists(Main.texturePath + "\\PS3\\Front\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS3\\Front\\" + gameSerial + ".jpg found");
            gameFront = LoadTexture(Main.texturePath + "\\PS3\\Front\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS3\\Front\\" + gameSerial + ".jpg not found");
            gameFront = null;
        }

        if (File.Exists(Main.texturePath + "\\PS3\\Back\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS3\\Back\\" + gameSerial + ".jpg found");
            gameBack = LoadTexture(Main.texturePath + "\\PS3\\Back\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS3\\Back\\" + gameSerial + ".jpg not found");
            gameBack = null;
        }

        SetSpine();
    }

    void SetSpine()
    {
        if (File.Exists(Main.texturePath + "\\PS3\\Spine\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS3\\Spine\\" + gameSerial + ".jpg found");
            gameSpine = LoadTexture(Main.texturePath + "\\PS3\\Spine\\" + gameSerial + ".jpg");
            customSpine = true;
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS3\\Spine\\" + gameSerial + ".jpg not found");

            if (caseType == "PS3 [Original]")
                gameSpine = spineOriginal;
            else if (caseType == "PS3 [Original-Alt]")
                gameSpine = spineOriginalAlt;
            else if (caseType == "PS3 [Original-Platinum]")
                gameSpine = spineOriginalPlatinum;
            else if (caseType == "PS3 [New]")
                gameSpine = spineNew;
            else if (caseType == "PS3 [New-Alt]" || caseType == "PS3 [New-Black]")
                gameSpine = spineNewAlt;
            else if (caseType == "PS3 [New-Platinum]")
                gameSpine = spineNewPlatinum;
            else if (caseType == "PS3 [New-Essentials]")
                gameSpine = spineNewEssentials;
            else
                gameSpine = spineOriginal;
        }
    }

    void SetColor()
    {
        if (caseType == "PS3 [Original]")
            gameTitleColor = textColorOriginal;
        else if (caseType == "PS3 [Original-Alt]")
            gameTitleColor = textColorOriginalAlt;
        else if (caseType == "PS3 [Original-Platinum]")
            gameTitleColor = textColorOriginalPlatinum;
        else if (caseType == "PS3 [New]")
            gameTitleColor = textColorNew;
        else if (caseType == "PS3 [New-Alt]" || caseType == "PS3 [New-Black]")
            gameTitleColor = textColorNewAlt;
        else if (caseType == "PS3 [New-Platinum]")
            gameTitleColor = textColorNewPlatinum;
        else if (caseType == "PS3 [New-Essentials]")
            gameTitleColor = textColorNewEssentials;
        else
            gameTitleColor = textColorOriginal;
    }

    void SetBoxColor()
    {
        if (caseType == "PS3 [Original-Platinum]")
            boxColor = boxColorPlatinum;
        else if (caseType == "PS3 [New-Platinum]")
            boxColor = boxColorPlatinum;
        else if (caseType == "PS3 [New-Black]")
            boxColor = boxColorBlack;
        else
            boxColor = boxColorStandard;
    }

    void SetBoxSmoothness()
    {
        if (caseType == "PS3 [Original-Platinum]")
            boxSmoothness = boxSmoothPlatinum;
        else if (caseType == "PS3 [New-Platinum]")
            boxSmoothness = boxSmoothPlatinum;
        else
            boxSmoothness = boxSmoothStandard;
    }
}
