using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PS2Game : MonoBehaviour
{
    [Header("Game Properties")]
    public string caseType = "";
    public float height, length, width;
    public string gameTitle, gameSerial, gameSubSerial;

    [Header("Box Properties")]
    public Texture gameFront;
    public Texture gameBack;
    public Texture gameSpine;
    public Color gameTitleColor;
    public Color boxColor;

    [Header("Components")]
    public Text gameTitle_text;
    public Text gameSerial_text;
    public Text gameSubSerial_text;
    public Material gameFront_mat, gameBack_mat, gameSpine_mat, gameBox_mat;
    public MeshRenderer meshRenderer;

    [Header("Spines")]
    public Texture spineStandard;
    public Texture spinePlatinum;
    public Texture spineSuperBustAMove;
    public Texture spinePlatinumRaymanRevolution;
    public Texture spineDemoDVD;
    public Texture spineJapanTheBest;

    [Header("Text Colors")]
    public Color textColorStandard;
    public Color textColorPlatinum;
    public Color textColorSuperBustAMove;
    public Color textColorPlatinumRaymanRevolution;
    public Color textColorSpiderMan3;
    public Color textColorDemoDVD;
    public Color textColorJapanTheBest;

    [Header("Box Colors")]
    public Color boxColorStandardBlue;
    public Color boxColorStandardBlack;
    public Color boxColorStandardOrange;
    public Color boxColorStandardWhite;
    public Color boxColorPlatinum;

    bool customSpine;

    internal void Set()
    {
        SetImages();
        SetColor();
        SetBoxColor();
        SetMaterials();
        SetText();
    }

    void SetMaterials()
    {
        meshRenderer.materials[0] = new Material(gameBox_mat);
        meshRenderer.materials[1] = new Material(gameSpine_mat);
        meshRenderer.materials[2] = new Material(gameBack_mat);
        meshRenderer.materials[3] = new Material(gameFront_mat);

        if (gameSpine)
            meshRenderer.materials[1].mainTexture = gameSpine;
        if (gameBack)
            meshRenderer.materials[2].mainTexture = gameBack;
        if (gameFront)
            meshRenderer.materials[3].mainTexture = gameFront;
        meshRenderer.materials[0].color = boxColor;
    }

    void SetText()
    {
        gameTitle_text.color = gameTitleColor;
        gameSerial_text.color = gameTitleColor;
        gameSubSerial_text.color = gameTitleColor;

        if (customSpine)
        {
            gameTitle_text.text = "";
            gameSerial_text.text = "";
            gameSubSerial_text.text = "";
        }

        else
        {
            gameTitle_text.text = gameTitle.Replace("\\n", "\n");
            gameSerial_text.text = gameSerial.Replace("-", "\n");
            gameSubSerial_text.text = gameSubSerial != "" ? gameSubSerial.PadLeft(7,'0') : "";
        }
    }

    Texture LoadTexture(string path)
    {
        Texture2D tex = null;

        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }

        return tex;
    }

    void SetImages()
    {
        if (File.Exists(Main.texturePath + "\\PS2\\Front\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS2\\Front\\" + gameSerial + ".jpg found");
            gameFront = LoadTexture(Main.texturePath + "\\PS2\\Front\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS2\\Front\\" + gameSerial + ".jpg not found");
            gameFront = null;
        }

        if (File.Exists(Main.texturePath + "\\PS2\\Back\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS2\\Back\\" + gameSerial + ".jpg found");
            gameBack = LoadTexture(Main.texturePath + "\\PS2\\Back\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS2\\Back\\" + gameSerial + ".jpg not found");
            gameBack = null;
        }

        SetSpine();
    }

    void SetSpine()
    {
        if (File.Exists(Main.texturePath + "\\PS2\\Spine\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\PS2\\Spine\\" + gameSerial + ".jpg found");
            gameSpine = LoadTexture(Main.texturePath + "\\PS2\\Spine\\" + gameSerial + ".jpg");
            customSpine = true;
        }
        else
        {
            Debug.Log(Main.texturePath + "\\PS2\\Spine\\" + gameSerial + ".jpg not found");

            if (caseType == "PS2 [Standard-Blue]")
                gameSpine = spineStandard;
            else if (caseType == "PS2 [Standard-Black]")
                gameSpine = spineStandard;
            else if (caseType == "PS2 [Standard-Orange]")
                gameSpine = spineStandard;
            else if (caseType == "PS2 [Standard-White]")
                gameSpine = spineStandard;
            else if (caseType == "PS2 [Super Bust-a-Move]")
                gameSpine = spineSuperBustAMove;
            else if (caseType == "PS2 [Platinum]" || caseType == "PS2 [Platinum-Blue]")
                gameSpine = spinePlatinum;
            else if (caseType == "PS2 [Platinum-RR]")
                gameSpine = spinePlatinumRaymanRevolution;
            else if (caseType == "PS2 [Platinum-SM3]")
                gameSpine = spinePlatinum;
            else if (caseType == "PS2 [PS2M-DemoDVD]")
                gameSpine = spineDemoDVD;
            else if (caseType == "PS2 [Japan-TheBest]")
                gameSpine = spineJapanTheBest;
            else
                gameSpine = spineStandard;
        }
    }

    void SetColor()
    {
        Debug.Log(caseType);
        if (caseType == "PS2 [Standard-Blue]")
            gameTitleColor = textColorStandard;
        else if (caseType == "PS2 [Standard-Black]")
            gameTitleColor = textColorStandard;
        else if (caseType == "PS2 [Standard-Orange]")
            gameTitleColor = textColorStandard;
        else if (caseType == "PS2 [Standard-White]")
            gameTitleColor = textColorStandard;
        else if (caseType == "PS2 [Super Bust-a-Move]")
            gameTitleColor = textColorSuperBustAMove;
        else if (caseType == "PS2 [Platinum]" || caseType == "PS2 [Platinum-Blue]")
            gameTitleColor = textColorPlatinum;
        else if (caseType == "PS2 [Platinum-RR]")
            gameTitleColor = textColorPlatinumRaymanRevolution;
        else if (caseType == "PS2 [Platinum-SM3]")
            gameTitleColor = textColorSpiderMan3;
        else if (caseType == "PS2 [PS2M-DemoDVD]")
            gameTitleColor = textColorDemoDVD;
        else if (caseType == "PS2 [Japan-TheBest]")
            gameTitleColor = textColorJapanTheBest;
        else
            gameTitleColor = textColorStandard;
    }

    void SetBoxColor()
    {
        if (caseType == "PS2 [Standard-Blue]")
            boxColor = boxColorStandardBlue;
        else if (caseType == "PS2 [Standard-Black]")
            boxColor = boxColorStandardBlack;
        else if (caseType == "PS2 [Standard-Orange]")
            boxColor = boxColorStandardOrange;
        else if (caseType == "PS2 [Standard-White]")
            boxColor = boxColorStandardWhite;
        else if (caseType == "PS2 [Super Bust-a-Move]")
            boxColor = boxColorStandardBlack;
        else if (caseType == "PS2 [Platinum]")
            boxColor = boxColorPlatinum;
        else if (caseType == "PS2 [Platinum-Blue]")
            boxColor = boxColorStandardBlue;
        else if (caseType == "PS2 [Platinum-RR]")
            boxColor = boxColorPlatinum;
        else if (caseType == "PS2 [Platinum-SM3]")
            boxColor = boxColorPlatinum;
        else if (caseType == "PS2 [PS2M-DemoDVD]")
            boxColor = boxColorStandardBlack;
        else if (caseType == "PS2 [Japan-TheBest]")
            boxColor = boxColorStandardBlack;
        else
            boxColor = boxColorStandardBlack;
    }
}
