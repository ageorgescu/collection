using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalCameraControl : MonoBehaviour
{
    Vector3 startRotation, startPosition;

    void Start()
    {
        startRotation = transform.localRotation.eulerAngles;
        startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
            transform.localRotation = Quaternion.Euler(startRotation.x, startRotation.y, startRotation.z);
        if (Input.GetKeyDown(KeyCode.C))
            transform.position = new Vector3(startPosition.x, startPosition.y, startPosition.z);
    }
}
