using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class WIIUGame : MonoBehaviour
{
    [Header("Game Properties")]
    public string caseType = "";
    public float height, length, width;
    public string gameTitle, gameSerial;

    [Header("Box Properties")]
    public Texture gameFront;
    public Texture gameBack;
    public Texture gameSpine;
    public Color gameTitleColor;
    public Color boxColor;

    [Header("Components")]
    public Text gameTitle_text;
    public Material gameFront_mat, gameBack_mat, gameSpine_mat, gameBox_mat;
    public MeshRenderer meshRenderer;

    [Header("Spines")]
    public Texture spineRed;
    public Texture spinePink;
    public Texture spineGreen;
    public Texture spineMagenta;
    public Texture spineOrange;
    public Texture spineLuigi;
    public Texture spineStandard;

    [Header("Text Colors")]
    public Color textColorStandard;

    [Header("Box Colors")]
    public Color boxColorBlue;
    public Color boxColorLuigi;

    bool customSpine;

    internal void Set()
    {
        SetImages();
        SetColor();
        SetBoxColor();
        SetMaterials();
        SetText();
    }

    void SetMaterials()
    {
        meshRenderer.materials[0] = new Material(gameBox_mat);
        meshRenderer.materials[1] = new Material(gameSpine_mat);
        meshRenderer.materials[2] = new Material(gameBack_mat);
        meshRenderer.materials[3] = new Material(gameFront_mat);

        if (gameSpine)
            meshRenderer.materials[1].mainTexture = gameSpine;
        if (gameBack)
            meshRenderer.materials[2].mainTexture = gameBack;
        if (gameFront)
            meshRenderer.materials[3].mainTexture = gameFront;
        meshRenderer.materials[0].color = boxColor;
    }

    void SetText()
    {
        gameTitle_text.color = gameTitleColor;

        if (customSpine)
            gameTitle_text.text = "";
        else
            gameTitle_text.text = gameTitle;
    }

    Texture LoadTexture(string path)
    {
        Texture2D tex = null;

        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }

        return tex;
    }

    void SetImages()
    {
        if (File.Exists(Main.texturePath + "\\WIIU\\Front\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\WIIU\\Front\\" + gameSerial + ".jpg found");
            gameFront = LoadTexture(Main.texturePath + "\\WIIU\\Front\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\WIIU\\Front\\" + gameSerial + ".jpg not found");
            gameFront = null;
        }

        if (File.Exists(Main.texturePath + "\\WIIU\\Back\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\WIIU\\Back\\" + gameSerial + ".jpg found");
            gameBack = LoadTexture(Main.texturePath + "\\WIIU\\Back\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\WIIU\\Back\\" + gameSerial + ".jpg not found");
            gameBack = null;
        }

        SetSpine();
    }

    void SetSpine()
    {
        if (File.Exists(Main.texturePath + "\\WIIU\\Spine\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\WIIU\\Spine\\" + gameSerial + ".jpg found");
            gameSpine = LoadTexture(Main.texturePath + "\\WIIU\\Spine\\" + gameSerial + ".jpg");
            customSpine = true;
        }
        else
        {
            Debug.Log(Main.texturePath + "\\WIIU\\Spine\\" + gameSerial + ".jpg not found");

            if (caseType == "WIIU [Red]")
                gameSpine = spineRed;
            else if (caseType == "WIIU [Pink]")
                gameSpine = spinePink;
            else if (caseType == "WIIU [Green]")
                gameSpine = spineGreen;
            else if (caseType == "WIIU [Magenta]")
                gameSpine = spineMagenta;
            else if (caseType == "WIIU [Orange]")
                gameSpine = spineOrange;
            else if (caseType == "WIIU [New Super Luigi U]")
                gameSpine = spineLuigi;
            else
                gameSpine = spineStandard;
        }
    }

    void SetColor()
    {
        Debug.Log(caseType);
        gameTitleColor = textColorStandard;
    }

    void SetBoxColor()
    {
        if (caseType == "WIIU [New Super Luigi U]")
            boxColor = boxColorLuigi;
        else
            boxColor = boxColorBlue;
    }
}
