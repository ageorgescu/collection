using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class WIIGame : MonoBehaviour
{
    [Header("Game Properties")]
    public string caseType = "";
    public float height, length, width;
    public string gameTitle, gameSerial;

    [Header("Box Properties")]
    public Texture gameFront;
    public Texture gameBack;
    public Texture gameSpine;
    public Color gameTitleColor;
    public Color boxColor;

    [Header("Components")]
    public Text gameTitle_text;
    public Material gameFront_mat, gameBack_mat, gameSpine_mat, gameBox_mat;
    public MeshRenderer meshRenderer;

    [Header("Spines")]
    public Texture spineRed;
    public Texture spinePink;
    public Texture spineMS2012;
    public Texture spineStandard;

    [Header("Text Colors")]
    public Color textColorStandard;

    [Header("Box Colors")]
    public Color boxColorWhite;
    public Color boxColorMS2012;

    bool customSpine;

    internal void Set()
    {
        SetImages();
        SetColor();
        SetBoxColor();
        SetMaterials();
        SetText();
    }

    void SetMaterials()
    {
        meshRenderer.materials[0] = new Material(gameBox_mat);
        meshRenderer.materials[1] = new Material(gameSpine_mat);
        meshRenderer.materials[2] = new Material(gameBack_mat);
        meshRenderer.materials[3] = new Material(gameFront_mat);

        if (gameSpine)
            meshRenderer.materials[1].mainTexture = gameSpine;
        if (gameBack)
            meshRenderer.materials[2].mainTexture = gameBack;
        if (gameFront)
            meshRenderer.materials[3].mainTexture = gameFront;
        meshRenderer.materials[0].color = boxColor;
    }

    void SetText()
    {
        gameTitle_text.color = gameTitleColor;

        if (customSpine)
            gameTitle_text.text = "";
        else
            gameTitle_text.text = gameTitle;
    }

    Texture LoadTexture(string path)
    {
        Texture2D tex = null;

        if (File.Exists(path))
        {
            byte[] fileData = File.ReadAllBytes(path);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
        }

        return tex;
    }

    void SetImages()
    {
        if (File.Exists(Main.texturePath + "\\WII\\Front\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\WII\\Front\\" + gameSerial + ".jpg found");
            gameFront = LoadTexture(Main.texturePath + "\\WII\\Front\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\WII\\Front\\" + gameSerial + ".jpg not found");
            gameFront = null;
        }

        if (File.Exists(Main.texturePath + "\\WII\\Back\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\WII\\Back\\" + gameSerial + ".jpg found");
            gameBack = LoadTexture(Main.texturePath + "\\WII\\Back\\" + gameSerial + ".jpg");
        }
        else
        {
            Debug.Log(Main.texturePath + "\\WII\\Back\\" + gameSerial + ".jpg not found");
            gameBack = null;
        }

        SetSpine();
    }

    void SetSpine()
    {
        if (File.Exists(Main.texturePath + "\\WII\\Spine\\" + gameSerial + ".jpg"))
        {
            Debug.Log(Main.texturePath + "\\WII\\Spine\\" + gameSerial + ".jpg found");
            gameSpine = LoadTexture(Main.texturePath + "\\WII\\Spine\\" + gameSerial + ".jpg");
            customSpine = true;
        }
        else
        {
            Debug.Log(Main.texturePath + "\\WII\\Spine\\" + gameSerial + ".jpg not found");

            if (caseType == "WII [Red]")
                gameSpine = spineRed;
            else if (caseType == "WII [Pink]")
                gameSpine = spinePink;
            else if (caseType == "WII [MS2012]")
                gameSpine = spineMS2012;
            else
                gameSpine = spineStandard;
        }
    }

    void SetColor()
    {
        Debug.Log(caseType);
        gameTitleColor = textColorStandard;
    }

    void SetBoxColor()
    {
        if (caseType == "WII [MS2012]")
            boxColor = boxColorMS2012;
        else
            boxColor = boxColorWhite;
    }
}
